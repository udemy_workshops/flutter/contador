import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final TextStyle _estiloTexto = TextStyle(
    fontSize: 25,
  );

  final int _conteo = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Título',
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Número de clicks:',
              style: _estiloTexto,
            ),
            Text(
              '$_conteo',
              style: _estiloTexto,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print('Hola Mundo!');
          // ++_conteo;
        },
        child: Icon(
          Icons.add,
        ),
      ),
    );
  }
}
